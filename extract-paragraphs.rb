require 'json'

output = File.open("paragraphs.txt", 'w')
json = JSON.parse(File.open("articles.json").read)
for article in json
  for paragraph in article["paragraphs"]
    output << "#{paragraph}\n"
  end
end
