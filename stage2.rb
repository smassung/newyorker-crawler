require 'open-uri'

File.open("urls.txt").each do |line|
  line.chomp!
  filename = line.gsub(/.*magazine\//, "").gsub(/\//, "_")
  puts "Crawling #{line}..."
  begin
    out = File.open("crawled/#{filename}.html", 'w')
    out << open(line).read
  rescue
    puts " -> Error!"
  end
  `sleep 3`
end
