require 'open-uri'
require 'nokogiri'

num_urls = 0
baseurl = "http://www.newyorker.com/magazine"
article_xpath = "//div//div//section//div//div//div//article//section//h2//a"
output = File.open("urls.txt", 'w')

Date.new(2004, 4, 12).upto(Date.new(2015, 4, 30)).select {|d| d.monday?}.each do
|date|
  month = date.month.to_s
  month = "0" + month if month.size == 1
  day = date.day.to_s
  day = "0" + day if day.size == 1
  puts "Crawling #{baseurl}/#{date.year}/#{month}/#{day}... (#{num_urls} total URLs)"
  begin
    html = Nokogiri::HTML(open("#{baseurl}/#{date.year}/#{month}/#{day}"))
    html.xpath(article_xpath).each do |article|
      output << "#{article['href']}\n"
      num_urls += 1
    end
    puts "Waiting..."
  rescue
    puts "Error!"
  end
  `sleep 3`
end
