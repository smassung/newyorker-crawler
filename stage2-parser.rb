require 'nokogiri'
require 'json'

output = []
nbsp = Nokogiri::HTML("&nbsp;").text

Dir.foreach("crawled/") do |doc|
  next if doc == "." or doc == ".."
  doc.chomp!
  puts doc
  html = Nokogiri::HTML(File.open("crawled/#{doc}"))
  next if html.blank?
  paragraphs = []
  begin
    html.css("div#articleBody").first.css("p").each do |slot|
      paragraphs << slot.text.gsub(/#{nbsp}/, " ").gsub(/\s+/, " ")
    end
  rescue
    puts " -> Parsing failed!"
    next
  end
  url = html.at('meta[property="og:url"]')['content']
  author = html.css(".author-details").first.at('meta[itemprop="name"]')['content'] rescue "N/A"
  title = html.at('meta[property="og:title"]')['content']
  desc = html.at('meta[property="og:description"]')['content'] rescue "N/A"
  output << { "url" => url, "author" => author, "title" => title,
    "description" => desc, "paragraphs" => paragraphs
  }
end

out = File.open("articles.json", 'w')
out << JSON.pretty_generate(output)
